# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import psycopg2
import datetime

class StoreToDatabasePipeline(object):

    def open_spider(self, spider):
        self.conn = psycopg2.connect(
            host="ec2-54-246-89-234.eu-west-1.compute.amazonaws.com",
            database="d2aije1i8tsl06",
            user="kqieqxdauvzytn",
            password="5193c523b8a64189e057b86fe86d50379634039c15534aae809194c175dbc05a")

    def close_spider(self, spider):
        self.conn.commit()
        self.conn.close()

    def process_item(self, item, spider):
        cur = self.conn.cursor()
        sql = "INSERT INTO ds_chart_pypiinfo(projects, releases, files, users, created_at, updated_at) VALUES(%s, %s, %s, %s, %s, %s)"

        projects = item.get('projects')
        releases = item.get('releases')
        files = item.get('files')
        users = item.get('users')
        
        created_at = datetime.datetime.now()
        updated_at = datetime.datetime.now()
        
        cur.execute(sql, (projects, releases, files, users, created_at, updated_at))
       
       # close the communication with the PostgreSQL
        cur.close()
        return item