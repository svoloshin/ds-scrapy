import scrapy

from scrapy.loader import ItemLoader
from ds.items import PyPiInfo


class ProtoSpider(scrapy.Spider):
    name = "pypi"

    def parse(self, response):

        l = ItemLoader(item=PyPiInfo(), response=response)
        
        def read_info(exp_arr):
            
            projects = None
            releases = None
            files = None
            users = None

            for info in exp_arr:
                if 'projects' in info:
                    projects = info.replace(',','').replace('projects','').replace(' ','')
                    projects = int(projects)
                
                if 'releases' in info:
                    releases = info.replace(',','').replace('releases','').replace(' ','')
                    releases = int(releases)

                if 'files' in info:
                    files = info.replace(',','').replace('files','').replace(' ','')
                    files = int(files)

                if 'users' in info:
                    users = info.replace(',','').replace('users','').replace(' ','')
                    users = int(users)

            return (projects, releases, files, users)

        exp_arr = response.css('div.statistics-bar').css('p.statistics-bar__statistic::text').getall();
        
        infos = read_info(exp_arr)

        self.log('INFOS!')
        self.log('projects! %s' % infos[0])
        self.log('releases! %s' % infos[1])
        self.log('files! %s' % infos[2])
        self.log('users! %s' % infos[3])
        
        l.add_value('projects', infos[0])
        l.add_value('releases', infos[1])
        l.add_value('files', infos[2])
        l.add_value('users', infos[3])

        return l.load_item()

    def start_requests(self):
        urls = [
            'https://pypi.org/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)