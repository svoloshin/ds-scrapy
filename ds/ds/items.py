# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst

class PyPiInfo(scrapy.Item):
    projects = scrapy.Field(output_processor=TakeFirst())
    releases = scrapy.Field(output_processor=TakeFirst())
    files = scrapy.Field(output_processor=TakeFirst())
    users = scrapy.Field(output_processor=TakeFirst())